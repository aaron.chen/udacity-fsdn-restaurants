from flask import Flask, render_template, url_for, request, redirect, jsonify
app = Flask(__name__)

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from database_setup import Base, Restaurant, MenuItem

engine = create_engine('sqlite:///restaurantmenu.db')
Base.metadata.bind = engine

DBsession = sessionmaker(bind=engine)
session = DBsession()

# Fake Restaurants
restaurant = {'name': 'The CRUDdy Crab', 'id': '1'}

restaurants = [
    {
        'name': 'The CRUDdy Crab',
        'id': '1'
    },
    {
        'name': 'Blue Burgers',
        'id': '2'
    },
    {
        'name': 'Taco Hut',
        'id': '3'
    }
]

# Fake Menu Items
items = [
    {
        'name': 'Cheese Pizza',
        'description': 'made with fresh cheese',
        'price': '$5.99',
        'course': 'Entree',
        'id': '1'
    },
    {
        'name': 'Chocolate Cake',
        'description': 'made with Dutch Chocolate',
        'price': '$3.99',
        'course': 'Dessert',
        'id': '2'
    },
    {
        'name': 'Caesar Salad',
        'description': 'with fresh organic vegetables',
        'price': '$5.99',
        'course': 'Entree',
        'id': '3'
    },
    {
        'name': 'Iced Tea',
        'description': 'with lemon',
        'price': '$.99',
        'course': 'Beverage',
        'id': '4'},
    {
        'name': 'Spinach Dip',
        'description': 'creamy dip with fresh spinach',
        'price': '$1.99',
        'course': 'Appetizer',
        'id': '5'}
]
item = {
    'name': 'Cheese Pizza',
    'description': 'made with fresh cheese',
    'price': '$5.99', 'course': 'Entree'
}

courses = ['Appetizer', 'Entree', 'Beverage', 'Dessert']


@app.route('/restaurants/JSON')
def showRestaurantsJSON():
    '''This page will show all my restaurants'''
    restaurants = session.query(Restaurant).all()
    return jsonify([restaurant.serialize for restaurant in restaurants])


@app.route('/')
@app.route('/restaurants')
def showRestaurants():
    '''This page will show all my restaurants'''
    restaurants = session.query(Restaurant).all()
    return render_template('restaurants.html', restaurants=restaurants)


@app.route('/restaurant/new', methods=['GET', 'POST'])
def newRestaurant():
    '''This page will be for making a new restaurant'''
    if request.method == 'POST':
        new_restaurant = Restaurant(name=request.form['name'])
        session.add(new_restaurant)
        session.commit()

        show_restaurants_url = url_for('showRestaurants')
        return redirect(show_restaurants_url)
    else:
        return render_template('newrestaurant.html')


@app.route('/restaurant/<int:restaurant_id>/edit', methods=['GET', 'POST'])
def editRestaurant(restaurant_id):
    '''This page will be for editing a restaurant'''
    editing_restaurant = session.query(
        Restaurant).filter_by(id=restaurant_id).one()

    if request.method == 'POST':
        new_restaurant_name = request.form['name']
        editing_restaurant.name = new_restaurant_name
        session.add(editing_restaurant)
        session.commit()

        show_restaurants_url = url_for('showRestaurants')
        return redirect(show_restaurants_url)
    else:
        return render_template('editrestaurant.html', restaurant_id=restaurant_id, restaurant_name=editing_restaurant.name)


@app.route('/restaurant/<int:restaurant_id>/delete', methods=['GET', 'POST'])
def deleteRestaurant(restaurant_id):
    '''This page will be for deleting a restaurant'''
    deleting_restaurant = session.query(
        Restaurant).filter_by(id=restaurant_id).one()

    if request.method == 'POST':
        session.delete(deleting_restaurant)
        session.commit()

        show_restaurants_url = url_for('showRestaurants')
        return redirect(show_restaurants_url)
    else:
        return render_template('deleterestaurant.html', restaurant_id=restaurant_id, restaurant_name=deleting_restaurant.name)


@app.route('/restaurant/<int:restaurant_id>/menu/JSON')
def showMenuJSON(restaurant_id):
    menu_items = session.query(MenuItem).filter_by(
        restaurant_id=restaurant_id).all()
    return jsonify([item.serialize for item in menu_items])


@app.route('/restaurant/<int:restaurant_id>')
@app.route('/restaurant/<int:restaurant_id>/menu')
def showMenu(restaurant_id):
    '''this page is for showing a restaurant menu'''
    restaurant = session.query(Restaurant).filter_by(id=restaurant_id).one()
    menu_items = session.query(MenuItem).filter_by(
        restaurant_id=restaurant_id).all()

    restaurant_name = restaurant.name
    course_dict = {}
    no_items = True

    for course in courses:
        course_dict[course] = []

    for item in menu_items:
        course = item.course
        course_dict[course].append(item)
        no_items = False

    return render_template('menu.html', restaurant_name=restaurant_name, restaurant_id=restaurant_id, courses=courses, course_dict=course_dict, no_items=no_items)

@app.route('/restaurant/<int:restaurant_id>/menu/<int:menu_id>/JSON')
def showMenuItemJSON(restaurant_id, menu_id):
    menu_item = session.query(MenuItem).filter_by(
        id=menu_id).one()
    return jsonify(menu_item.serialize)

@app.route('/restaurant/<int:restaurant_id>/menu/new', methods=['GET', 'POST'])
def newMenuItem(restaurant_id):
    '''This page is for making a new menu item for a restaurant'''

    if request.method == 'POST':
        item_name = request.form['name']
        item_description = request.form['description']
        item_price = request.form['price']
        item_course = request.form['course']

        new_menu_item = MenuItem(
            restaurant_id=restaurant_id, name=item_name, description=item_description, price=item_price, course=item_course)
        session.add(new_menu_item)
        session.commit()

        show_menu_url = url_for('showMenu', restaurant_id=restaurant_id)
        return redirect(show_menu_url)
    else:
        return render_template('newmenuitem.html', restaurant_id=restaurant_id)


@app.route('/restaurant/<int:restaurant_id>/menu/<int:menu_id>/edit', methods=['GET', 'POST'])
def editMenuItem(restaurant_id, menu_id):
    '''This page is for editing a menu item'''

    editing_item = session.query(MenuItem).filter_by(id=menu_id).one()
    if request.method == 'POST':
        new_item_name = request.form['name']

        editing_item.name = new_item_name
        session.add(editing_item)
        session.commit()

        show_menu_url = url_for('showMenu', restaurant_id=restaurant_id)
        return redirect(show_menu_url)
    else:
        return render_template('editmenuitem.html', restaurant_id=restaurant_id, menu_id=menu_id, item_name=editing_item.name)


@app.route('/restaurant/<int:restaurant_id>/menu/<int:menu_id>/delete', methods=['GET', 'POST'])
def deleteMenuItem(restaurant_id, menu_id):
    '''This page is for deleting a menu item'''

    deleting_item = session.query(MenuItem).filter_by(id=menu_id).one()
    if request.method == 'POST':
        session.delete(deleting_item)
        session.commit()

        show_menu_url = url_for('showMenu', restaurant_id=restaurant_id)
        return redirect(show_menu_url)
    else:
        return render_template('deletemenuitem.html', restaurant_id=restaurant_id, menu_id=menu_id, item_name=deleting_item.name)


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
