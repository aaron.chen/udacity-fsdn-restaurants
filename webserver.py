#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from http.server import HTTPServer, BaseHTTPRequestHandler
import cgi
import bleach
import re

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from database_setup import Base, Restaurant

session = None

new_restaurant_form = """
<form
    method='POST'
    enctype='multipart/form-data'
    action='/restaurants/new'
>
    <h2>Make a New Restaurant</h2>
    <input name='restaurant_name' type='text'>
    <input type='submit' value='Create'>
</form>
"""

edit_restaurant_form = """
<form
    method='POST'
    enctype='multipart/form-data'
    action='/restaurant/{}/edit'
>
    <h2>{}</h2>
    <input name='restaurant_name' type='text' placeholder='{}'>
    <input type='submit' value='Rename'>
</form>
"""

delete_restaurant_form = """
<form
    method='POST'
    enctype='multipart/form-data'
    action='/restaurant/{}/delete'
>
    <h2>Are you sure you want to delete {}?</h2>
    <input type='submit' value='Delete'>
</form>
"""


class WebserverHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        try:
            if self.path == '/restaurants':
                self.send_response(200, 'OK')
                self.send_header('Content-Type', 'text/html')
                self.end_headers()

                output = """
                <html>
                    <body>
                    <a href='/restaurants/new'>Make a New Restaurant Here</a>
                    <br/><br/>
                """

                restaurant_rows = session.query(Restaurant).all()
                for restaurant in restaurant_rows:
                    restaurant_name = bleach.clean(restaurant.name)
                    restaurant_id = restaurant.id

                    output += restaurant_name
                    output += "<br/>"
                    output += "<a href='restaurant/{}/edit'>Edit<a/>".format(
                        restaurant_id)
                    output += "<br/>"
                    output += "<a href='restaurant/{}/delete'>Delete<a/>".format(
                        restaurant_id)
                    output += "<br/><br/>"
                output += """
                    </body>
                <html>
                """
                self.wfile.write(output.encode())
                print(output)
            elif self.path == '/restaurants/new':
                self.send_response(200, 'OK')
                self.send_header('Content-Type', 'text/html')
                self.end_headers()

                output = """
                <html>
                    <body>
                """
                output += new_restaurant_form
                output += """
                    </body>
                </html>
                """
                self.wfile.write(output.encode())
                print(output)
            elif re.match('^/restaurant/[0-9]+/edit$', self.path) is not None:
                id = int(self.path.split('/')[2])

                restaurant_query = session.query(
                    Restaurant).filter_by(id=id).first()
                if restaurant_query is not None:
                    self.send_response(200, 'OK')
                    self.send_header('Content-Type', 'text/html')
                    self.end_headers()

                    restaurant = restaurant_query
                    restaurant_name = bleach.clean(restaurant.name)

                    output = """
                    <html>
                        <body>
                    """
                    output += edit_restaurant_form.format(
                        id, restaurant_name, restaurant_name)
                    output += """
                        </body>
                    </html>
                    """

                    self.wfile.write(output.encode())
                    print(output)
            elif re.match('^/restaurant/[0-9]+/delete$', self.path) is not None:
                id = int(self.path.split('/')[2])

                restaurant_query = session.query(
                    Restaurant).filter_by(id=id).first()
                if restaurant_query is not None:
                    self.send_response(200, 'OK')
                    self.send_header('Content-Type', 'text/html')
                    self.end_headers()

                    restaurant = restaurant_query
                    restaurant_name = bleach.clean(restaurant.name)

                    output = """
                    <html>
                        <body>
                    """
                    output += delete_restaurant_form.format(
                        id, restaurant_name)
                    output += """
                        </body>
                    </html>
                    """

                    self.wfile.write(output.encode())
                    print(output)
        except IOError:
            self.send_error(404, 'File not found {}'.format(self.path))

    def do_POST(self):
        if self.path == '/restaurants/new':
            self.send_response(303)
            self.send_header('Location', '/restaurants')
            self.end_headers()

            ctype, pdict = cgi.parse_header(
                self.headers['Content-Type']
            )
            pdict['boundary'] = bytes(pdict['boundary'], 'utf-8')
            if ctype == 'multipart/form-data':
                fields = cgi.parse_multipart(self.rfile, pdict)
                restaurant_name = fields.get('restaurant_name')[0].decode()

            new_restaurant = Restaurant(name=restaurant_name)
            session.add(new_restaurant)
            session.commit()
        elif re.match('^/restaurant/[0-9]+/edit$', self.path) is not None:
            id = int(self.path.split('/')[2])
            edited_restaurant = session.query(
                Restaurant).filter_by(id=id).one()
            self.send_response(303)
            self.send_header('Location', '/restaurants')
            self.end_headers()

            ctype, pdict = cgi.parse_header(self.headers['Content-Type'])
            pdict['boundary'] = bytes(pdict['boundary'], 'utf-8')
            if ctype == 'multipart/form-data':
                fields = cgi.parse_multipart(self.rfile, pdict)
                restaurant_name = fields.get('restaurant_name')[0].decode()

            edited_restaurant.name = restaurant_name
            session.add(edited_restaurant)
            session.commit()
        elif re.match('^/restaurant/[0-9]+/edit$', self.path) is not None:
            id = int(self.path.split('/')[2])
            edited_restaurant = session.query(
                Restaurant).filter_by(id=id).one()
            self.send_response(303)
            self.send_header('Location', '/restaurants')
            self.end_headers()

            ctype, pdict = cgi.parse_header(self.headers['Content-Type'])
            pdict['boundary'] = bytes(pdict['boundary'], 'utf-8')
            if ctype == 'multipart/form-data':
                fields = cgi.parse_multipart(self.rfile, pdict)
                restaurant_name = fields.get('restaurant_name')[0].decode()

            edited_restaurant.name = restaurant_name
            session.add(edited_restaurant)
            session.commit()
        elif re.match('^/restaurant/[0-9]+/delete$', self.path) is not None:
            id = int(self.path.split('/')[2])
            deleted_restaurant = session.query(
                Restaurant).filter_by(id=id).one()
            self.send_response(303)
            self.send_header('Location', '/restaurants')
            self.end_headers()

            session.delete(deleted_restaurant)
            session.commit()

def setup_ORM():
    try:
        engine = create_engine('sqlite:///restaurantmenu.db')
        Base.metadata.bind = engine
        DBSession = sessionmaker(bind=engine)
        return DBSession()
    except Exception as e:
        print("Could not set up ORM")
        print(e)


def main():
    try:
        global session
        session = setup_ORM()

        port = 8080
        server = HTTPServer(('', port), WebserverHandler)
        print('Web server running on port {}'.format(port))
        server.serve_forever()
    except KeyboardInterrupt:
        print('^C entered, stopping web server...')
        server.socket.close()


if __name__ == '__main__':
    main()
